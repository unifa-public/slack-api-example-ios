# Slack API for iOS

## 準備

- Xcode 8.3.3
- CocoaPods 1.3.1

## CocoaPods

```sh
$ pod install
$ open SlackApiExample.xcworkspace
```

## Blog

- http://tech.unifa-e.com/entry/2017/01/11/153752

