//
//  Model.swift
//  SlackApiExample
//
//  Created by Kazuya Shida on 2017/11/06.
//  Copyright © 2017 Kazuya Shida. All rights reserved.
//

import Foundation
import ObjectMapper

struct Emoji: Mappable {
    var ok: Bool = false
    var list: [String: String] = [:]
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        ok   <- map["ok"]
        list <- map["emoji"]
    }
}

struct PostMessage: Mappable {
    var ok: Bool = false
    var ts: String = ""
    var channel: String = ""
    var message: AnyObject?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        ok      <- map["ok"]
        ts      <- map["ts"]
        channel <- map["channel"]
        message <- map["message"]
    }
}

struct FileMessage: Mappable {
    var ok: Bool = false
    var file: AnyObject?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        ok   <- map["ok"]
        file <- map["file"]
    }
}
