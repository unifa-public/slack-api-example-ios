//
//  Slack.swift
//  SlackApiExample
//
//  Created by Kazuya Shida on 2017/11/06.
//  Copyright © 2017 Kazuya Shida. All rights reserved.
//

import Alamofire
import ObjectMapper
import RxSwift
import RxCocoa

struct Client {
    static let manager = SessionManager.default
    
    private static func json(
        method: HTTPMethod = .get,
        api: API,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: [String: String]? = nil) -> Observable<Any> {
        
        if api.data == nil {
            return dataRequest(method: method, api: api, encoding: encoding, headers: headers)
                .flatMap { (dataRequest) -> Observable<Any> in
                    return manager.rx.json(request: dataRequest)
            }
        } else {
            return uploadRequest(api: api, headers: headers)
                .flatMap { (request) -> Observable<Any> in
                    return manager.rx.json(request: request)
            }
        }
    }
    
    private static func dataRequest(method: HTTPMethod = .get, api: API, encoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) -> Observable<DataRequest> {
        return Observable<DataRequest>
            .create { (observer) -> Disposable in
                let url = api.buildURL
                let request = manager.request(url, method: method, parameters: api.parameters, encoding: encoding, headers: headers)
                observer.onNext(request)
                observer.onCompleted()
                return Disposables.create()
        }
    }
    
    static func uploadRequest(
        method: HTTPMethod = .post,
        api: API,
        headers: [String: String]? = nil) -> Observable<UploadRequest> {
        
        return Observable<UploadRequest>
            .create { (observer) -> Disposable in
                let url = api.buildURL
                if let data = api.data {
                    manager.upload(multipartFormData: { (formData) in
                        if let filename = api.parameters["filename"] as? String {
                            formData.append(data, withName: "file", fileName: filename, mimeType: "image/png")
                        }
                        for param in api.parameters {
                            if let value = param.value as? String, let data = value.data(using: .utf8) {
                                formData.append(data, withName: param.key)
                            }
                        }
                    }, to: url, headers: headers, encodingCompletion: { (result) in
                        switch result {
                        case .success(request: let request, streamingFromDisk: _, streamFileURL: _):
                            observer.onNext(request)
                            observer.onCompleted()
                            break
                        case .failure(let error):
                            observer.onError(error)
                            break
                        }
                    })
                } else {
                    observer.onError(RxCocoaURLError.unknown)
                }
                return Disposables.create()
        }
    }
    
    /// レスポンスのJSON形式がDictionary(JSONObject)の場合
    static func get<T: Mappable>(api: API, encoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) -> Observable<T> {
        return Client.json(method: .get, api: api, encoding: encoding, headers: headers).map(mapping())
    }
    
    /// レスポンスのJSON形式がArray(JSONArray)の場合
    static func get<T: Mappable>(api: API, encoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) -> Observable<[T]> {
        return Client.json(method: .get, api: api, encoding: encoding, headers: headers).map(mappingToArray())
    }
    
    static func post<T: Mappable>(api: API, encoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) -> Observable<T> {
        return Client.json(method: .post, api: api, encoding: encoding, headers: headers).map(mapping())
    }
    
    /// JSONからオブジェクトへのマッピング
    static func mapping<T: Mappable>() -> ((Any) -> T) {
        return { (json) -> T in
            let item: T = Mapper<T>().map(JSONObject: json)!
            return item
        }
    }
    
    /// JSONからArrayタイプのオブジェクトへマッピング
    static func mappingToArray<T: Mappable>() -> ((Any) -> [T]) {
        return { (json) -> [T] in
            let item: [T] = Mapper<T>().mapArray(JSONObject: json) ?? []
            return item
        }
    }
}

protocol API {
    
    /// リクエストのURL(e.g. https://slack.com/api/emoji.list)
    var buildURL: URL { get }
    
    /// APIのベースURL(e.g. https://slack.com/api)
    var baseURL: String { get }
    
    /// APIのパス(e.g. /emoji.list)
    var path: String { get }
    
    /// リクエストのパラメーター(e.g. token=xoxp-xxx)
    var parameters: [String: Any] { get }
    
    var data: Data? { get }
}

enum SlackApi: API {
    
    /// 絵文字リストの取得
    case emojiList
    
    /// メッセージを送信
    case chatPostMessage(text: String, channel: String, options: [String: String])
    
    /// :
    
    /// ファイルのアップロード
    case fileUpload(filename: String, file: Data, options: [String: String])
    
    static let token = "xoxp"
    
    var buildURL: URL {
        return URL(string: "\(baseURL)\(path)")!
    }
    
    var baseURL: String {
        return "https://slack.com/api"
    }
    
    var path: String {
        switch self {
        case .emojiList:                                        return "/emoji.list"
        case .chatPostMessage(text: _, channel: _, options: _): return "/chat.postMessage"
        case .fileUpload(filename: _, file: _, options: _):     return "/files.upload"
        }
    }
    
    var parameters: Parameters {
        var params = ["token": SlackApi.token]
        switch self {
        case .emojiList:
            return params
        case .chatPostMessage(text: let text, channel: let channel, options: let options):
            params["text"] = text
            params["channel"] = channel
            options.forEach { (k, v) in params[k] = v }
            return params
        case .fileUpload(filename: let filename, file: _, options: let options):
            params["filename"] = filename
            for (key, value) in options {
                params[key] = value
            }
            return params
        }
    }
    
    var data: Data? {
        if case .fileUpload(filename: _, file: let data, options: _) = self {
            return data
        }
        return nil
    }
}

extension Alamofire.SessionManager: ReactiveCompatible {
}

extension Reactive where Base: SessionManager {
    
    func json(request: DataRequest) -> Observable<Any> {
        return Observable<Any>.create { (observer) -> Disposable in
            #if DEBUG
                NSLog("%@, request=%@", #function, request.debugDescription)
            #endif
            let req = request.responseJSON(completionHandler: { (response) in
                #if DEBUG
                    NSLog("%@, response=%@", #function, response.debugDescription)
                #endif
                guard let res = response.response, let json = response.result.value else {
                    if let error = response.result.error {
                        observer.onError(error)
                    } else {
                        observer.onError(RxCocoaURLError.unknown)
                    }
                    return
                }
                if 200 ..< 300 ~= res.statusCode {
                    observer.onNext(json)
                    observer.onCompleted()
                } else {
                    let error = RxCocoaURLError.httpRequestFailed(response: res, data: response.data)
                    observer.onError(error)
                }
            })
            return Disposables.create {
                req.cancel()
            }
        }
    }
}
