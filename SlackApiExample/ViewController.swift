//
//  ViewController.swift
//  SlackApiExample
//
//  Created by Kazuya Shida on 2017/11/06.
//  Copyright © 2017 Kazuya Shida. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func emoji(sender: UIButton) {
        let request: Observable<Emoji> = Client.get(api: SlackApi.emojiList)
        request
            .subscribe(
                onNext: { (emoji) in
                    print(emoji)
            },
                onError: { (error) in
                    print(error)
            },
                onCompleted: {
            })
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func postMessage(sender: UIButton) {
        let api = SlackApi.chatPostMessage(
            text: "てすと",
            channel: "general",
            options: ["username": "Bot", "icon_emoji": ":robot_face:"]
        )
        let request: Observable<PostMessage> = Client.post(api: api)
        request
            .subscribe(
                onNext: { (message) in
                    print(message)
            },
                onError: { (error) in
                    print(error)
            },
                onCompleted: {
            })
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func postScreenshot(sender: UIButton) {
        guard let image = screenshot(), let data = UIImagePNGRepresentation(image) else {
            return
        }
        
        let api = SlackApi.fileUpload(filename: "screenshot", file: data, options: ["channels": "general"])
        let request: Observable<FileMessage> = Client.post(api: api)
        request
            .subscribe(
                onNext: { (message) in
                    print(message)
            },
                onError: { (error) in
                    print(error)
            },
                onCompleted: {
            })
            .addDisposableTo(disposeBag)
    }
    
    func screenshot() -> UIImage? {
        if let window = UIApplication.shared.keyWindow {
            let rect = UIScreen.main.bounds
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            context?.fill(rect)
            window.layer.render(in: context!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
        return nil
    }
}
